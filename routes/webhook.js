var express = require('express');
var router = express.Router();

/* GET webhook listing. */
router.get('/', function(req, res, next) {
  if (req.query['hub.verify_token'] === "EAACjKzEWNVUBAPqZAj1YbJZAhSxuNcYUFjtRB76xXkFfPpN3PcpB8OLZAfIxeQqpcNrepoiwt8YC0yGcBeQ8us3CuZCkJ16IFFYZAi6DB860fZAoXFZBbHDaUCVZA9WhhRPctcPJvgNTTyPOPzx3oFz21DZBavqZCILpY9drp5bqCEGQZDZD") {
    res.send(req.query['hub.challenge']);
  } else {
    res.send('Error, wrong validation token');
  }
});

module.exports = router;
